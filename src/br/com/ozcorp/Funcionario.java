package br.com.ozcorp;

public class Funcionario {

	// atributos
	protected String titulo;
	protected double salarioBase;
	protected String nome;
	protected String rg;
	protected String cpf;
	protected String matricula;
	protected String senha;
	private Sexo sexo;
	private TipoSangue tipoSanguineo;
	protected String cargo;
	protected String email;
	private NivelAcesso nivelAcesso;

	// construtor
	public Funcionario(String titulo, double salarioBase, String nome, String rg, String cpf, String matricula,
			String senha, Sexo sexo, String cargo, String email, TipoSangue tipoSanguineo, NivelAcesso nivelAcesso) {
		this.titulo = titulo;
		this.cpf = cpf;
		this.matricula = matricula;
		this.nome = nome;
		this.salarioBase = salarioBase;
		this.rg = rg;
		this.senha = senha;
		this.sexo = sexo;
		this.cargo = cargo;
		this.email = email;
		this.tipoSanguineo = tipoSanguineo;
		this.nivelAcesso = nivelAcesso;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public NivelAcesso getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(NivelAcesso nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	public TipoSangue getTipoSanguineo() {
		return tipoSanguineo;
	}

	public void setTipoSanguineo(TipoSangue tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

}
