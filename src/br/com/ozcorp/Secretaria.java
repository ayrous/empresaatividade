package br.com.ozcorp;

public class Secretaria extends Funcionario {

	public Secretaria(String titulo, double salarioBase, String nome, String rg, String cpf, String matricula,
			String senha, Sexo sexo, String cargo, String email, TipoSangue tipoSanguineo, NivelAcesso nivelAcesso) {
		super(titulo, salarioBase, nome, rg, cpf, matricula, senha, sexo, cargo, email, tipoSanguineo, nivelAcesso);

	}

	//ATRIBUTOS
	final String sigla = "SCRT";
	final String departamento = "Secretaria";
	
	//getters and setters
	public String getSigla() {
		return sigla;
	}
	public String getDepartamento() {
		return departamento;
	}
	
}
