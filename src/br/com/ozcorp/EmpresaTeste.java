package br.com.ozcorp;

import javax.swing.JOptionPane;

public class EmpresaTeste {

	public static void main(String[] args) {

		Financeiro iris = new Financeiro("Produtiva", 7890, "Iris", "098768", "987-98", "8878089", "1223", Sexo.FEMININO,	"Gerente Financeiro", "iris_campanella@hotmail.com", TipoSangue.ABMENOS, NivelAcesso.GERENTE);
		JOptionPane.showMessageDialog(null,  "Nome: " + iris.getNome() + "\nTitulo: " + iris.getTitulo() +  "\nRG: " + iris.getRg() +  "\nMatr�cula: " + iris.getMatricula() +  "\nCpf: " + iris.getCpf() +  "\nSal�rio base: " + iris.getSalarioBase() +  "\nSenha: " + iris.getSenha() +  "\nCargo: " + iris.getCargo() +  "\nDepartamento: " + iris.getDep() +  "\nSigla de seu Departamento: " + iris.getSigla() +  "\nSexo: " + iris.getSexo().sexo +  "\nEmail: " + iris.getEmail() +  "\nTipo Sangu�neo: " + iris.getTipoSanguineo().ts +  "\nN�vel de Acesso: " + iris.getNivelAcesso().nivel,iris.getNome(), JOptionPane.INFORMATION_MESSAGE);

		
		Gerencia mario = new Gerencia("Millor", 9000.90, "Mario Andrade", "90886-33", "898978-88", "i8893", "9909",
				Sexo.MASCULINO, "Analista", "mario.and@yahoo.com", TipoSangue.ABMENOS, NivelAcesso.ANALISTA);
		JOptionPane.showMessageDialog(null, "Nome: " + mario.getNome() +  "\nTitulo: " + mario.getTitulo() +  "\nRG: " + mario.getRg() +  "\nMatr�cula: " + mario.getMatricula()  + "\nCpf: " + mario.getCpf()  + "\nSal�rio base: " + mario.getSalarioBase()  + "\nSenha: " + mario.getSenha()  + "\nCargo: " + mario.getCargo()  +  "\nDepartamento: " + mario.getDepartamento() +  "\nSigla de seu Departamento: " + mario.getSigla() +  "\nSexo: " + mario.getSexo().sexo +  "\nEmail: " + mario.getEmail() +  "\nTipo Sangu�neo: " + mario.getTipoSanguineo().ts +  "\nN�vel de Acesso: " + mario.getNivelAcesso().nivel, mario.getNome(), JOptionPane.INFORMATION_MESSAGE);

		
		Diretoria flavia = new Diretoria("Promovida", 9988.89, "Fl�via Nascimento", "89873-22", "9839-092", "099389",
				"ijendn", Sexo.FEMININO, "Diretora Geral", "flavia.luisa@hotmail.br", TipoSangue.ONEGATIVO, NivelAcesso.DIRETOR);
		JOptionPane.showMessageDialog(null, "Nome: " + flavia.getNome() +  "\nTitulo: " + flavia.getTitulo() +  "\nRG: " + flavia.getRg() +  "\nMatr�cula: " + flavia.getMatricula() +  "\nCpf: " + flavia.getCpf() +  "\nSal�rio base: " + flavia.getSalarioBase() +  "\nSenha: " + flavia.getSenha() +  "\nCargo: " + flavia.getCargo() +  "\nDepartamento: " + flavia.getDepartamento() +  "\nSigla de seu Departamento: " + flavia.getSigla()  + "\nSexo: " + flavia.getSexo().sexo +  "\nEmail: " + flavia.getEmail() +  "\nTipo Sangu�neo: " + flavia.getTipoSanguineo().ts +  "\nN�vel de acesso: " + flavia.getNivelAcesso().nivel,flavia.getNome(), JOptionPane.INFORMATION_MESSAGE);

		
		Secretaria georgia = new Secretaria("MAJOR", 8907.77, "Ge�rgia Maria", "21342423-32", "8908d", "9082n9c", "89!@9d3e", Sexo.FEMININO, "Secret�ria", "yahoo.com.br", TipoSangue.OPOSITIVO, NivelAcesso.SECRETARIO);
		JOptionPane.showMessageDialog(null, "Nome: " + georgia.getNome() + "\nTitulo: " + georgia.getTitulo() +  "\nRG: " + georgia.getRg() + "\nMatr�cula: " + georgia.getMatricula() + "\nCpf: " + georgia.getCpf() + "\nSal�rio base: " + georgia.getSalarioBase() + "\nSenha: " + georgia.getSenha() + "\nCargo: " + georgia.getCargo() +  "\nDepartamento: " + georgia.getDepartamento() + "\nSigla de seu Departamento: " + georgia.getSigla() + "\nSexo: " + georgia.getSexo().sexo +  "\nEmail: " + georgia.getEmail() +  "\nTipo Sangu�neo: " + georgia.getTipoSanguineo().ts +  "\nN�vel de acesso: " + georgia.getNivelAcesso().nivel, georgia.getNome(), JOptionPane.INFORMATION_MESSAGE);
	
		
		Financeiro john = new Financeiro("Confian�a", 3490, "John Melo", "009898768", "2234987-98", "887234089", "179S3", Sexo.MASCULINO,	"Engenheiro Financeiro", "john_mello@gmail.com", TipoSangue.ABMENOS, NivelAcesso.ENGENHEIRO);
		JOptionPane.showMessageDialog(null,  "Nome: " + john.getNome() + "\nTitulo: " + john.getTitulo()  +  "\nRG: " + john.getRg() +  "\nMatr�cula: " + john.getMatricula() +  "\nCpf: " + john.getCpf() +  "\nSal�rio base: " + john.getSalarioBase() +  "\nSenha: " + john.getSenha() +  "\nCargo: " + john.getCargo() +  "\nDepartamento: " + john.getDep() +  "\nSigla de seu Departamento: " + john.getSigla() +  "\nSexo: " + john.getSexo().sexo +  "\nEmail: " + john.getEmail() +  "\nTipo Sangu�neo: " + john.getTipoSanguineo().ts +  "\nN�vel de Acesso: " + john.getNivelAcesso().nivel, john.getNome(), JOptionPane.INFORMATION_MESSAGE);

	}

}