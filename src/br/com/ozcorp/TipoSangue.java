package br.com.ozcorp;

public enum TipoSangue {
	ABMAIS("AB+"), ABMENOS("AB-"), OPOSITIVO("O+"), ONEGATIVO("O-");
	
	public String ts;
	
	TipoSangue(String ts){
		this.ts = ts;
	}

}
