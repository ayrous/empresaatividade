package br.com.ozcorp;

public enum NivelAcesso {
	DIRETOR("0"), SECRETARIO("1"), GERENTE("2"), ENGENHEIRO("3"), ANALISTA("4");
	
	public String nivel;
	
	NivelAcesso(String nivel){
		this.nivel = nivel;
	}
}
