package br.com.ozcorp;

public class Diretoria extends Funcionario {

	public Diretoria(String titulo, double salarioBase, String nome, String rg, String cpf, String matricula,
			String senha, Sexo sexo, String cargo, String email, TipoSangue tipoSanguineo, NivelAcesso nivelAcesso) {
		super(titulo, salarioBase, nome, rg, cpf, matricula, senha, sexo, cargo, email, tipoSanguineo, nivelAcesso);

	}

	// atributos
	final String sigla = "DRT";
	final String departamento = "Diretoria";

	// getters and setters
	public String getSigla() {
		return sigla;
	}

	public String getDepartamento() {
		return departamento;
	}

}
