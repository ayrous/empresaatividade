package br.com.ozcorp;

public class Financeiro extends Funcionario {

	public Financeiro(String titulo, double salarioBase, String nome, String rg, String cpf, String matricula,
			String senha, Sexo sexo, String cargo, String email, TipoSangue tipoSanguineo, NivelAcesso nivelAcesso) {
		super(titulo, salarioBase, nome, rg, cpf, matricula, senha, sexo, cargo, email, tipoSanguineo, nivelAcesso);

	}

	// ATRIBUTOS
	final String sigla = "FNC";
	final String dep = "Financeiro";

	// getters and setters
	public String getSigla() {
		return sigla;
	}

	public String getDep() {
		return dep;
	}

}
